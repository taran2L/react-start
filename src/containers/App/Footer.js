import React, {Fragment} from "react";

export default () => (
	<Fragment>
		<div id="logo-bg" />
		<div id="gradient" />
		<div id="footer">
			<div className="text catamaran">If you experience any bugs or require help please send a message to “PolyAlpha.io Assistant”.</div>
		</div>
	</Fragment>
);

