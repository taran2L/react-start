import React from "react";

export const MainTitle = ({children}) => (
	children && (<h1 className="main-title raleway">{children}</h1>)
);
